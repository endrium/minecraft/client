package net.minecraft.client.gui;

import net.minecraft.realms.RealmsBridge;
import net.minecraft.util.ResourceLocation;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;

public class GuiIngameMenu extends GuiScreen {
    private int visibleTime;

    private FontRenderer endriumFontRenderer;

    /**
     * Adds the buttons (and other controls) to the screen in question. Called when the GUI is displayed and when the
     * window resizes, the buttonList is cleared beforehand.
     */
    public void initGui() {
        this.buttonList.clear();

        this.buttonList.add(new GuiButton(0, this.width / 2 - 75, this.height / 2 + 24 + -16, 150, 20, "RETOUR AU JEU"));
        this.buttonList.add(new GuiButton(1, this.width / 2 - 75, this.height / 2 + 48 + -16, 150, 20, "OPTIONS"));
        this.buttonList.add(new GuiButton(2, this.width / 2 - 75, this.height / 2 + 72 + -16, 150, 20, "VOTER"));
        this.buttonList.add(new GuiButton(3, this.width / 2 - 75, this.height / 2 + 96 + -16, 150, 20, "DECONNEXION"));
    }

    /**
     * Called by the controls from the buttonList when activated. (Mouse pressed for buttons)
     */
    protected void actionPerformed(GuiButton button) throws IOException {
        switch (button.id) {
            case 0:
                this.mc.displayGuiScreen(null);
                this.mc.setIngameFocus();
                break;

            case 1:
                this.mc.displayGuiScreen(new GuiOptions(this, this.mc.gameSettings));
                break;

            case 2:
                try {
                    URI url = new URI("https://endrium.fr/vote");

                    if (Desktop.isDesktopSupported()) {
                        Desktop.getDesktop().browse(url);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case 3:
                boolean flag = this.mc.isIntegratedServerRunning();
                boolean flag1 = this.mc.isConnectedToRealms();

                button.enabled = false;

                this.mc.world.sendQuittingDisconnectingPacket();
                this.mc.loadWorld(null);

                if (flag) {
                    this.mc.displayGuiScreen(new GuiMainMenu());
                } else if (flag1) {
                    RealmsBridge realmsbridge = new RealmsBridge();
                    realmsbridge.switchToRealms(new GuiMainMenu());
                } else {
                    this.mc.displayGuiScreen(new GuiMultiplayer(new GuiMainMenu()));
                }
                break;

            default:
                break;
        }
    }

    /**
     * Called from the main game loop to update the screen.
     */
    public void updateScreen() {
        super.updateScreen();

        ++this.visibleTime;
    }

    /**
     * Draws the screen and all the components in it.
     */
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        this.drawDefaultBackground();

        this.mc.getTextureManager().bindTexture(new ResourceLocation("textures/blocks/dirt.png"));
        this.drawTexturedModalRect(this.width / 2 - 75, this.height / 4 + -32, 0, 0, 32, 32);

        this.drawCenteredString(this.fontRenderer, "MENU DU JEU", this.width / 2, this.height / 2 + -16, 16777215);

        super.drawScreen(mouseX, mouseY, partialTicks);
    }
}
